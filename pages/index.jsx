import Head from 'next/head'
import Header from "../components/modules/header";
import { Card, Container, Row, Col} from "react-bootstrap";


export default function Home() {
    return (
        <div>

            <Header/>
            <Head>
                <title>AVR Challenge</title>
                <meta name="description" content="AVR Challenge" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className="bg-secondary text-light p-5 rounded-lg vw-100 mb-4">
                <Container >
                    <h1 className="display-4">AVR competition 2024</h1>
                    <p className="lead">2nd Automated Vulnerability Research Challenge</p>
                </Container>
            </div>
            <Container style={{marginBottom: '30px'}}>
                <Card>
                    <Card.Body>
                        <Card.Title>  </Card.Title>
                        <Card.Text>
                            <p>
                            <Row>
                                <Col sm = {2}><strong>⏰ When: </strong></Col>
                                <Col >FEBRUARY - JUNE 2024</Col>
                            </Row>
                            <Row>
                                <Col sm = {2}><strong>🎮 Players: </strong></Col>
                                <Col >Student teams from Dutch Academic Universities or other interested Dutch organizations</Col>
                            </Row>
                            <Row>
                                <Col sm = {2}><strong>🏆 Rewards: </strong></Col>
                                <Col > On top of the honors 1st, 2nd and 3rd placed team will receive a monetary prize! The team that manages to place 1st will win <strong>€ 6.000,-</strong>!</Col>
                            </Row>
                            <Row>
                                <Col sm = {2}><strong>✉️ To register: </strong></Col>
                                <Col > <a href="https://forms.gle/YWk4uu9sHJUuVCzRA">Signup form</a>  </Col>
                            </Row>
                            </p>
                            <p>
                            Inspired by the DARPA Cyber Grand Challenge and building on the 1st AVR Challenge, as a team (max 4 students) you are challenged to design and build or improve a system for automated discovery of security vulnerabilities. The Challenge has two phases:  
                            </p>
                            <p>
                                <h5><u>First phase:</u> your team will design, build, and test your vulnerability finder. </h5>
                                <ul>
                                    <li> You will be provided with the necessary tools and instructions to achieve this. </li>
                                    <li> Support material will be provided. </li>
                                    <li> You will have the opportunity to attend workshops with experts in the field.</li>
                                    <li> Linkage with university courses may vary between universities. </li>
                                    <li> The phase will be scheduled by your university to align with its academic calendar. </li>
                                </ul>
                                <h5><u>Second Phase:</u> your team will compete against other teams. </h5>
                                <ul>
                                    <li> Before Saturday May 24th: submit the source code of your vulnerability finder. *</li>
                                    <li> Between May 27th and May 28th, the submitted AVR tools will be run against test targets </li>
                                    <li> Until Friday May 31st your team will have the opportunity to do the last tweaks before final submission at 17:00 pm </li>
                                    <li>     Between June 1st and June 6th: the submitted AVR tools will be tested and scored, based on how well and quickly they are able to identify unique vulnerabilities in the target programs. </li>
                                    <li>June 6th: final Day of the AVR Challenge, all teams are expected to be present. During the day we will provide an interesting mix of presentations, networking opportunities and updates on the latest developments in the final hours of the challenge. At the end of the day the winner of the 2nd AVR Challenge will be announced.</li>
                                </ul>
                            </p>
                            <p>
                            Stay tuned for more detailed information, prizes, and updates via the website! 
                            </p>
                            <p>
                                Interested in participating in the AVR Challenge? Please fill out our <a href="https://forms.gle/YWk4uu9sHJUuVCzRA"> signup form </a>!
                            </p>
                            <p><sub>
                            * It will be possible to do the submission and test run in the week ending May 24th to allow for exams in the week 27th-31st of May. The tool must then be submitted Friday May 17th.
                            </sub></p>
                        </Card.Text>
                        <Container style={{display: "flex", width: "100%", justifyContent: "space-around"}}>
                            <img src="/avrlogos.png"/>
                        </Container>
                    </Card.Body>
                </Card>
            </Container>
 
        </div>
    )
}
