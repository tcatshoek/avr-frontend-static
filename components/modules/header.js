import {Container, Navbar } from "react-bootstrap";

import Link from "next/link"

export default function Header() {
    return (
        <Navbar bg="dark" variant="dark" expand="lg" sticky="top" className="py-1">
            <Container>
                <Link href="/" passHref>
                    <Navbar.Brand>AVR Competition</Navbar.Brand>
                </Link>
            </Container>
        </Navbar>
    )
}
